def update_data(data,key,new_data,add_index={}):
    if not key in data:
        data[key] = {}
    if None in data:
        data.pop(None)
    data[key].update(parse_data(new_data,add_index))

def parse_data(value,add_index={}):
    data = None
    if type(value)!=dict:
        data = {None: value}
    else:
        data = dict_to_vector(value)
    for pos,val in add_index.items():
        data = {((val,)
            if k is None else (val,k)
            if type(k) is int and pos==0 else (k,val)
            if type(k) is int else (k[:pos]+(val,)+k[pos:])):v
            for k,v in data.items()}
    return data

def dict_to_vector(value):
    ddict = {}
    for k,v in value.items():
        if type(v)==dict:
            for k2,v2 in dict_to_vector(v).items():
                if type(k2)==tuple:
                    ddict[(k,*k2)] = v2
                else:
                    ddict[(k,k2)] = v2
        else:
            ddict = value
    return ddict

def drop_values(values,filters):
    '''
    drop_values({(1,1):4,(1,2):2,(2,3):5},{1:[1]})
    -> {(1):4,(2):2}
    '''
    temp = values
    for col,filter in filters.items():
        temp = {tuple(e
            for i,e in enumerate(tup) if i!=col-1) if len(tuple(e
                for i,e in enumerate(tup) if i!=col-1))>1 else tuple(e
                    for i,e in enumerate(tup) if i!=col-1)[0]:val
            for tup,val in temp.items() if tup[col-1] in filter}
    return temp

def filter_values(values,filters):
    '''
    filter_values({(1,1):4,(1,2):2,(1,3):5},{2:[1,2]})
    -> {(1,1):4,(1,2):2}
    '''
    temp = values
    for col,filter in filters.items():
        temp = {tup:val
            for tup,val in temp.items() if tup[col-1] in filter}
    return temp

def dict_index_items(dict):
    out = []
    for key in dict.keys():
        len_key = len(key) if type(key) is tuple else 1
        for item in range(len_key):
            key_item = key[item] if type(key) is tuple else key
            if item == len(out):
                out.append([])
            if key_item not in out[item]:
                out[item].append(key_item)
    return out

def reset_index(values,columns):
    '''
    reset_index({(1,3):1,(1,4):2,(2,7):3,(2,2):4},[1])
    {(1, 1): 1, (1, 2): 2, (2, 3): 3, (2, 4): 4}
    '''

    index_orig = dict_index_items(values)
    index_alt = {col:{} for col in columns}
    for col in columns:
        base_num = 1
        for value in index_orig[col]:
            index_alt[col][value] = base_num
            base_num +=1

    values_alt = {}
    for key,value in values.items():
        key_alt = tuple([index_alt[idx][val] if idx in columns else val for idx,val in enumerate(key)])
        values_alt[key_alt] = value

    return values_alt

def index_max(dict):
    max_val = -1
    idx_max = -1
    for key,val in dict.items():
        if val > max_val:
            max_val = val
            idx_max = key
    return idx_max

def index_min(dict):
    min_val = None
    idx_max = -1
    for key,val in dict.items():
        if min_val is None:
            min_val = val
            idx_max = key
        if val < min_val:
            min_val = val
            idx_max = key
    return idx_max

from pyomo.environ import *
def map_solve(self,instance):
    opt = SolverFactory('gurobi')
    opt.options['NumericFocus'] = 3
    opt.options['Quad'] = 1
    opt.options['Presolve'] = 2
    status = opt.solve(instance)
    return instance, status

def moco(a):
    b=a*2
    c=a*3
    return b,c

# temp = {(1, 1): 1, (2, 1): 0}
# filter_values(temp,{2:1})
# values = temp
# filters={2:1}
