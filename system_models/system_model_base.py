import pandas as pd
import numpy as np

class SystemModelBase():
    def __init__(self):
        self.data = None
        self.lines = { #Dim (Property,L)
            'From':             {},
            'To':               {},
            'Installed':        {},
            'InvestmentCost':   {},
            'MaxFlow':          {},
            'Reactance':        {},
        }
        self.generators = { #Dim (Property,N)
            'Node':                 {},
            'VariableCost':         {},
            'MaxCapacity':          {},
            'MinStableLevel':       {},
            'CostReserveHoldUp':    {},
            'CostReserveHoldDown':  {},
            'MaxReserveHoldUp':     {},
            'MaxReserveHoldDown':   {},
        }
        self.nodes = {
            'load': # DIM (N,T)
                {},
            'sample_duration': # DIM (T)
                {},
            }
        self.cost_lost_load = None
        self.cost_generation_shift = None
        self.contingencies = {
            'Probability': # DIM (S,T)
                {},
            'Generators': # DIM (I,S)
                {},
            'Lines': # DIM (L,S)
                {},
            }
        self.outagerates = {
            'Generators': # DIM (T,I)
                {},
            'Lines': # DIM (T,L)
                {},
        }

    def load_from_excel(self, excel_file):
        xls = pd.ExcelFile(excel_file)
        for sheet_name in xls.sheet_names:
            sheet_data = pd.read_excel(xls,sheet_name)
            sheet_data.set_index(
                sheet_data[sheet_data.columns[0]],
                inplace=True)
            del sheet_data[sheet_data.columns[0]]

            if sheet_name == 'load':
                self.nodes['load'] = (
                self._filter_empty(sheet_data.to_dict()))
            elif sheet_name == 'sample_duration':
                self.nodes['sample_duration'] = (
                self._filter_empty(sheet_data.to_dict()))
            elif sheet_name == 'lines':
                self.lines = (
                self._filter_empty(sheet_data.to_dict()))
            elif sheet_name == 'generators':
                self.generators = (
                self._filter_empty(sheet_data.to_dict()))
            elif sheet_name == 'cost_lost_load':
                self.cost_lost_load = (
                self._filter_empty(sheet_data.to_dict()))
            elif sheet_name == 'cost_generation_shift':
                self.cost_generation_shift = (
                self._filter_empty(sheet_data.to_dict()))
            elif sheet_name == 'contingencies.probability':
                self.contingencies['Probability'] = (
                self._filter_empty(sheet_data.to_dict()))
            elif sheet_name == 'contingencies.generators':
                self.contingencies['Generators'] = (
                self._filter_empty(sheet_data.to_dict()))
            elif sheet_name == 'contingencies.lines':
                self.contingencies['Lines'] = (
                self._filter_empty(sheet_data.to_dict()))
            elif sheet_name == 'outagerate.generators':
                self.outagerates['Generators'] = (
                self._filter_empty(sheet_data.to_dict()))
            elif sheet_name == 'outagerate.lines':
                self.outagerates['Lines'] = (
                self._filter_empty(sheet_data.to_dict()))

    def _filter_empty(self,dict_val):
        if type(dict_val) is not dict:
            return dict_val
        if '-' in dict_val:
            return self._filter_empty(dict_val['-'])
        return {
            idx:(val['-'] if '-' in val else val)
            for idx,val in dict_val.items()}

    def probability_from_outagerates(self, style = 'conditional'):
        I = list(self.generators['Node'].keys())
        L = list(self.lines['From'].keys())
        T = list(self.nodes['sample_duration'].keys())
        S = list(self.contingencies['Generators'][1].keys())
        new_outages = {}
        if style == 'marginal':
            self._marginal_outrates()
        self.contingencies['Probability'] = {s:{
            t: self._independent_probability(s,t) for t in T
        } for s in S}

    def _independent_probability(self,s,t):
        I = list(self.generators['Node'].keys())
        L = list(self.lines['From'].keys())
        T = list(self.nodes['sample_duration'].keys())
        S = list(self.contingencies['Generators'][1].keys())
        generators = np.prod([(self.contingencies['Generators'][g][s] -
            (self.contingencies['Generators'][g][s] * 2 - 1) *
            self.outagerates['Generators'][t][g]) for g in I])
        lines =  np.prod([(self.contingencies['Lines'][l][s] -
            (self.contingencies['Lines'][l][s] * 2 - 1) *
            self.outagerates['Lines'][t][l]) for l in L])
        return generators * lines

    def _marginal_outrates(self):
        I = list(self.generators['Node'].keys())
        L = list(self.lines['From'].keys())
        T = list(self.nodes['sample_duration'].keys())
        S = list(self.contingencies['Generators'][1].keys())
        self.outagerates['Generators'] = {t:{
            g: sum(
                self.outagerates['Generators'][t][g] *
                self.nodes['sample_duration'][t] for t in T)/sum(self.nodes['sample_duration'][t] for t in T) for g in I
        } for t in T}
        self.outagerates['Lines'] = {t:{
            l: sum(
                self.outagerates['Lines'][t][l] *
                self.nodes['sample_duration'][t] for t in T)/sum(self.nodes['sample_duration'][t] for t in T) for l in L
        } for t in T}

if __name__ == '__main__':
    print('Testing...')
    import os
    self = SystemModelBase()
    excel_file = os.path.join('system_models','IEEE24Busbar','RTS_1.xlsx')
    self.load_from_excel(excel_file)
    self.probability_from_outagerates(style = 'marginal')
    self.contingencies['Probability']
    self.contingencies['Generators'][1].keys()
    self.outagerates['Generators']
