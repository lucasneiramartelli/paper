import os
import cloudpickle

class ModelBase():
    def __init__(self):
        self.solution = {
            'instance': None,
            'status': None,
            'gap': None,
            'time': None
        }

    def save_solution(self):
        solution_file = self.solution_file()
        dirname = os.path.dirname(solution_file)
        if not os.path.exists(dirname):
            os.makedirs(dirname, exist_ok=True)
        with open(solution_file, 'wb') as f:  # Python 3: open(..., 'wb')
            cloudpickle.dump([self.solution,self.settings], f)

    def read_solution(self):
        with open(self.solution_file(),'rb') as f:  # Python 3: open(..., 'rb')
            solution,settings = cloudpickle.load(f)
        return solution,settings

    def solution_file(self):
        model = self.settings['model_name']
        data = self.settings['data_name']
        return os.path.join('solutions',model,data+'.pso')
