import pandas as pd

from utils import parse_data

class TestSmallSystem2Gen():
    def __init__(self):
        self.data = None
        self.lines = { #Dim (L)
            'From':             {1:1,2:1},
            'To':               {1:2,2:2},
            'Installed':        {1:1,2:0},
            'InvestmentCost':   {1:0,2:20000},
            'MaxFlow':          {1:100,2:100},
            'Reactance':        {1:0.1,2:0.1},
        }
        self.generators = { #Dim (N)
            'Node':                 {1:1,2:2},
            'VariableCost':         {1:30,2:200},
            'MaxCapacity':          {1:200,2:200},
            'MinStableLevel':       {1:5,2:0},
            'CostReserveHoldUp':    {1:30,2:200},
            'CostReserveHoldDown':  {1:30,2:200},
            'MaxReserveHoldUp':     {1:0,2:0},
            'MaxReserveHoldDown':   {1:0,2:0},
        }
        self.nodes = {
            'load': { # DIM (N,T)
                1:{1:0},
                2:{1:200},
                },
            'sample_duration': # DIM (T)
                {1:1},
            }
        self.cost_lost_load = 500
        self.cost_generation_shift = 0
        self.contingencies = {
            'Probability': { # DIM (S,T)
                1: {1:0.1},
                2: {1:0.08}
                },
            'Generators':{ # DIM (I,S)
                1: {1:1,2:0},
                2: {1:0,2:1},
                },
            'Lines':{ # DIM (L,S)
                1: {1:1,2:1},
                2: {1:1,2:1},
                },
            }
        self._initialize_data()

    def _initialize_data(self):
        real_data = {}
        real_data['Ie'] = parse_data(
            len(self.generators['Node']))
        real_data['Te'] = parse_data(
            len(self.nodes['sample_duration']))
        real_data['Le'] = parse_data(
            len(self.lines['Installed']))
        real_data['Se'] = parse_data(
            len(self.contingencies['Probability']))
        real_data['Ne'] = parse_data(
            len(self.nodes['load']))
        real_data['LTo'] = parse_data(
            self.lines['To'])
        real_data['LFrom'] = parse_data(
            self.lines['From'])
        real_data['LInstalled'] = parse_data(
            self.lines['Installed'])
        real_data['INode'] = parse_data(
            self.generators['Node'])
        real_data['CostVariable'] = parse_data(
            self.generators['VariableCost'])
        real_data['CostReserveHoldUp'] = parse_data(
            self.generators['CostReserveHoldUp'])
        real_data['CostReserveHoldDown'] = parse_data(
            self.generators['CostReserveHoldDown'])
        real_data['MaxReserveHoldUp'] = parse_data(
            self.generators['MaxReserveHoldUp'])
        real_data['MaxReserveHoldDown'] = parse_data(
            self.generators['MaxReserveHoldDown'])
        real_data['MaxCapacity'] = parse_data(
            self.generators['MaxCapacity'])
        real_data['MinStableLevel'] = parse_data(
            self.generators['MinStableLevel'])
        real_data['CostGenerationShift'] = parse_data(
            self.cost_generation_shift)
        real_data['MaxFlow'] = parse_data(
            self.lines['MaxFlow'])
        real_data['Reactance'] = parse_data(
            self.lines['Reactance'])
        real_data['CostTransmissionInvestment'] = parse_data(
            self.lines['InvestmentCost'])
        real_data['ProbabilityOccurrence'] = parse_data(
            self.contingencies['Probability'])
        real_data['ContingencyStatusGenerator'] = parse_data(
            self.contingencies['Generators'])
        real_data['ContingencyStatusLine'] = parse_data(
            self.contingencies['Lines'])
        real_data['CostLostLoad'] = parse_data(
            self.cost_lost_load)
        real_data['Load'] = parse_data(
            self.nodes['load'])
        real_data['SampleDuration'] = parse_data(
            self.nodes['sample_duration'])
        self.data = {None: real_data}

if __name__ == '__main__':
    print('Testing...')
    self = TestSmallSystem()

    self.data
