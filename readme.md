## Thesis repository
This code typed on Python3 contains all optimization models and data employed
on my thesis. There are a lot of mistakes on this work, but there are
also some learnings.

## Structure
The main executable code is optimizer.py.

Optimization models (algorithms) are stored on optimization_models/.

Optimization inputs (data) are stored on system_models/.

## Instructions

## Requirements

## Thesis link

## Contact
Lucas Neira Martelli
lucas.neira@ug.uchile.cl
