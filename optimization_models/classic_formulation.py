from __future__ import division
from pyomo.environ import *
import time

from optimization_models.model_base import ModelBase

class ClassicFormulation(ModelBase):
    '''
    This is the monolithic formulation for stochastic security constraint
    planning model.
    '''
    def __init__(self, BigM = 10000, stout = True, varout = False):
        '''
        --
        BigM: parameter to linealize constraints, should be bigger than LHS
        (left hand size) value of constraint.
        stout: (boolean) Indicates if algorithm steps will be printed on
        console.
        '''
        super().__init__()
        self.stout = stout
        self.varout = varout
        self.settings = {
            'BigM':BigM,
            'model_name': 'ClassicFormulation',
            'data_name': None,
        }
        self.opt_models = {
            'investment_operation_risk': None
        }
        self._initialize_models()

    def _initialize_models(self):
        self._investment_operation_risk_model()

    def execute(self,datasource,solver='gurobi'):
        '''
        Methodology application (Optimization), can be an algorithm
        with multiple optimization models involved.
        --
        data: dictionary defined on system_models/
        solver: optimization solver
        '''
        self.settings['data_name'] = datasource.__class__.__name__
        instance = (
            self.opt_models['investment_operation_risk'].create_instance(
                data=datasource.data))
        # MILP Solve
        time_i = time.time()
        opt = SolverFactory(solver)
        status = opt.solve(instance)
        time_f = time.time()
        assert status.solver.termination_condition == 'optimal'
        if self.stout:
            print('Status: {}'.format(status.solver.termination_condition))

        # # MILP Solve
        # instance.BinCommit.fix()
        # for l in instance.L:
        #     if instance.LInstalled[l]==0:
        #         instance.BinInvestment[l].fix()
        # status = opt.solve(instance)
        # assert status.solver.termination_condition == 'optimal'

        self.solution['instance'] = instance
        self.solution['gap'] = 0
        self.solution['status'] = 'optimal'
        self.solution['time'] = time_f-time_i
        self.save_solution()

        # Temporal results summary
        if self.stout:
            print('Status: {}'.format(status.solver.termination_condition))
            print('Objective: ',value(instance.Obj))
            print('Operation: ',sum(sum(
                instance.SampleDuration[t] *
                (instance.CostVariable[i] * instance.Generation[i,t].value +
                instance.CostReserveHoldUp[i] * instance.ReserveHoldUp[i,t].value +
                instance.CostReserveHoldDown[i] * instance.ReserveHoldDown[i,t].value) +
                0 for i in instance.I) for t in instance.T))
            print('Risk: ',sum(sum(
                instance.SampleDuration[t] *
                instance.ProbabilityOccurrence[s,t] * (
                    instance.CostLostLoad * sum(
                        instance.LostLoad[s,b,t].value for b in instance.N) +
                    instance.CostGenerationShift * sum(
                        instance.GenerationShift[s,i,t].value for i in instance.I)
                ) +
                0 for t in instance.T) for s in instance.S if instance.Se!=0)
            )
            print('Investment: ',sum(instance.CostTransmissionInvestment[l] * instance.BinInvestment[l].value +
                0 for l in instance.L if instance.LInstalled[l]==0))
            print('Investment: ',{i:v
                for i,v in instance.BinInvestment.extract_values().items()
                if v is not None and v >0.95})
            print('LostLoad: ',sum(instance.LostLoad.extract_values().values()))
            print('GenerationShift: ',sum(instance.GenerationShift.extract_values().values()))
        if self.varout:
            print('Generation:')
            for i in instance.I:
                print('Unit {}'.format(i),['{0:3.0f}'.format(instance.Generation[i,t].value) for t in instance.T])
            print('Reserves Up:')
            for i in instance.I:
                print('Unit {}'.format(i),['{0:3.0f}'.format(instance.ReserveHoldUp[i,t].value) for t in instance.T])
            print('Reserves Down:')
            for i in instance.I:
                print('Unit {}'.format(i),['{0:3.0f}'.format(instance.ReserveHoldDown[i,t].value) for t in instance.T])
            # print('Marginal cost:')
            # for b in instance.N:
            #     print(
            #         'Node {}'.format(b),
            #         ['{0:03.0f}'.format(
            #             instance.dual.get(instance.ConstraintBalance[b,t]) /
            #             instance.SampleDuration[t]
            #             ) for t in instance.T])

    def _investment_operation_risk_model(self):
        '''
        Model definition. This model co-optimize Investment costs, Operation
        costs and Risk acording to a stochastic security constraint formulation.
        '''
        model = AbstractModel()

        ## Number of elements per Set
        model.Ie = Param(within=NonNegativeIntegers)
        model.Te = Param(within=NonNegativeIntegers)
        model.Le = Param(within=NonNegativeIntegers)
        model.Se = Param(within=NonNegativeIntegers)
        model.Ne = Param(within=NonNegativeIntegers)

        ## Sets
        model.I = RangeSet(1, model.Ie)
        model.T = RangeSet(1, model.Te)
        model.S = RangeSet(1, model.Se)
        model.L = RangeSet(1, model.Le)
        model.N = RangeSet(1, model.Ne)
        ## Subsets
        model.LTo = Param(model.L)
        model.LFrom = Param(model.L)
        model.LInstalled = Param(model.L)
        model.INode = Param(model.I)

        ## System Parameters
        # Generators
        model.CostVariable = Param(model.I)
        model.CostReserveHoldUp = Param(model.I)
        model.CostReserveHoldDown = Param(model.I)
        model.MaxReserveHoldUp = Param(model.I)
        model.MaxReserveHoldDown = Param(model.I)
        model.MaxCapacity = Param(model.I)
        model.MinStableLevel = Param(model.I)
        model.CostGenerationShift = Param() #Curtailment mejor, no se
        # Lines
        model.MaxFlow = Param(model.L)
        model.Reactance = Param(model.L)
        model.CostTransmissionInvestment = Param(model.L)
        # Contingences
        model.ProbabilityOccurrence = Param(model.S,model.T)
        model.ContingencyStatusGenerator = Param(model.I,model.S)
        model.ContingencyStatusLine = Param(model.L,model.S)
        # Load
        model.CostLostLoad = Param()
        model.Load = Param(model.N,model.T)
        model.SampleDuration = Param(model.T)

        ## Variables
        # Generators
        model.Generation = Var(model.I,model.T, domain=NonNegativeReals)
        model.BinCommit = Var(model.I,model.T, domain=Binary)
        model.ReserveHoldUp = Var(model.I,model.T, domain=NonNegativeReals)
        model.ReserveHoldDown = Var(model.I,model.T, domain=NonNegativeReals)
        model.ContingencyGeneration = Var(model.S,model.I,model.T, domain=NonNegativeReals)
        model.ContingencyReserveHoldUp = Var(model.S,model.I,model.T, domain=NonNegativeReals)
        model.ContingencyReserveHoldDown = Var(model.S,model.I,model.T, domain=NonNegativeReals)
        model.GenerationShift = Var(model.S,model.I,model.T, domain=NonNegativeReals)
        # Lines
        model.Flow = Var(model.L,model.T)
        model.Theta = Var(model.N,model.T)
        model.BinInvestment = Var(model.L, domain=Binary)
        model.ContingencyFlow = Var(model.S,model.L,model.T)
        model.ContingencyTheta = Var(model.S,model.N,model.T)
        # Load
        model.LostLoad = Var(model.S,model.N,model.T, domain=NonNegativeReals)

        ## Objective Function
        def fo_handler(model):
            return (
                sum(sum(
                    model.SampleDuration[t] *
                    (model.CostVariable[i] * model.Generation[i,t] +
                    model.CostReserveHoldUp[i] * model.ReserveHoldUp[i,t] +
                    model.CostReserveHoldDown[i] * model.ReserveHoldDown[i,t]) +
                    0 for i in model.I) for t in model.T) +
                sum(model.CostTransmissionInvestment[l] * model.BinInvestment[l] +
                    0 for l in model.L if model.LInstalled[l]==0) +
                sum(sum(
                    model.SampleDuration[t] *
                    model.ProbabilityOccurrence[s,t] * (
                        model.CostLostLoad * sum(
                            model.LostLoad[s,b,t] for b in model.N) +
                        model.CostGenerationShift * sum(
                            model.GenerationShift[s,i,t] for i in model.I)
                    ) +
                    0 for t in model.T) for s in model.S if model.Se!=0)
                )
        model.Obj = Objective(rule=fo_handler, sense=minimize)

        ## Constraints
        # Without contingencies constraints
        def balance_generation_load(model, b, t):
            return (
                sum(model.Generation[i,t] for i in model.I if model.INode[i]==b) ==
                sum(model.Flow[l,t] for l in model.L if model.LFrom[l]==b) -
                sum(model.Flow[l,t] for l in model.L if model.LTo[l]==b) +
                model.Load[b,t]
                )
        model.ConstraintBalance = Constraint(model.N,model.T, rule=balance_generation_load)

        def reserve_hold_up(model, i, t):
            return (
                model.ReserveHoldUp[i,t] <=
                model.MaxReserveHoldUp[i] * model.BinCommit[i,t]
                )
        model.ConstraintReserveHoldUp = Constraint(model.I,model.T, rule=reserve_hold_up)

        def reserve_hold_down(model, i, t):
            return (
                model.ReserveHoldDown[i,t] <=
                model.MaxReserveHoldDown[i] * model.BinCommit[i,t]
                )
        model.ConstraintReserveHoldDown = Constraint(model.I,model.T, rule=reserve_hold_down)

        def generation_upper_bound(model, i, t):
            return (
                model.Generation[i,t] + model.ReserveHoldUp[i,t] <=
                model.MaxCapacity[i] * model.BinCommit[i,t]
                )
        model.ConstraintGenerationUpperBound = Constraint(model.I,model.T, rule=generation_upper_bound)

        def generation_lower_bound(model, i, t):
            return (
                model.Generation[i,t] - model.ReserveHoldDown[i,t] >=
                model.MinStableLevel[i] * model.BinCommit[i,t]
                )
        model.ConstraintGenerationLowerBound = Constraint(model.I,model.T, rule=generation_lower_bound)

        def flow_upper_bound(model, l, t):
            if model.LInstalled[l]==1:
                return (
                    model.Flow[l,t] <= model.MaxFlow[l]
                    )
            elif model.LInstalled[l]==0:
                return (
                    model.Flow[l,t] <= model.MaxFlow[l] * model.BinInvestment[l]
                    )
        model.ConstraintFlowUpperBound = Constraint(model.L,model.T, rule=flow_upper_bound)

        def flow_lower_bound(model, l, t):
            if model.LInstalled[l]==1:
                return (
                    model.Flow[l,t] >= - model.MaxFlow[l]
                    )
            elif model.LInstalled[l]==0:
                return (
                    model.Flow[l,t] >= - model.MaxFlow[l] * model.BinInvestment[l]
                    )
        model.ConstraintFlowLowerBound = Constraint(model.L,model.T, rule=flow_lower_bound)

        def theta_upper_bound(model, l, t):
            if model.LInstalled[l]==1:
                return (
                    model.Flow[l,t] <= (
                        model.Theta[model.LTo[l],t] -
                        model.Theta[model.LFrom[l],t]
                        ) / model.Reactance[l]
                    )
            elif model.LInstalled[l]==0:
                return (
                    model.Flow[l,t] - (
                        model.Theta[model.LTo[l],t] -
                        model.Theta[model.LFrom[l],t]
                        ) / model.Reactance[l] <=
                        self.settings['BigM'] * (1-model.BinInvestment[l])
                    )
        model.ConstraintThetaUpperBound = Constraint(model.L,model.T, rule=theta_upper_bound)

        def theta_lower_bound(model, l, t):
            if model.LInstalled[l]==1:
                return (
                    model.Flow[l,t] >= (
                        model.Theta[model.LTo[l],t] -
                        model.Theta[model.LFrom[l],t]
                        ) / model.Reactance[l]
                    )
            elif model.LInstalled[l]==0:
                return (
                    model.Flow[l,t] - (
                        model.Theta[model.LTo[l],t] -
                        model.Theta[model.LFrom[l],t]
                        ) / model.Reactance[l] >=
                        - self.settings['BigM'] * (1-model.BinInvestment[l])
                    )
        model.ConstraintThetaLowerBound = (
            Constraint(model.L,model.T, rule=theta_lower_bound))

        # Contingency constraints
        def contingency_balance_generation_load(model, s, b, t):
            if model.Se !=0:
                return (
                    sum(model.ContingencyGeneration[s,i,t] for i in model.I if model.INode[i]==b) ==
                    sum(model.ContingencyFlow[s,l,t] for l in model.L if model.LFrom[l]==b) -
                    sum(model.ContingencyFlow[s,l,t] for l in model.L if model.LTo[l]==b) -
                    model.LostLoad[s,b,t] +
                    sum(model.GenerationShift[s,i,t] for i in model.I if model.INode[i]==b) +
                    model.Load[b,t]
                    )
            else:
                return Constraint.Feasible
        model.ConstraintContingencyBalance = (
            Constraint(model.S,model.N,model.T, rule=contingency_balance_generation_load))

        def contingency_lost_load_upper_bound(model, s, b, t):
            if model.Se !=0:
                return (
                    model.LostLoad[s,b,t] <=
                    model.Load[b,t]
                    )
            else:
                return Constraint.Feasible
        model.ConstraintContingencyLostLoadUpperBound = (
            Constraint(model.S,model.N,model.T, rule=contingency_lost_load_upper_bound))

        def contingency_generation_shift_upper_bound(model, s, i, t):
            if model.Se !=0:
                return (
                    model.GenerationShift[s,i,t] <=
                    model.Generation[i,t]
                    )
            else:
                return Constraint.Feasible
        model.ConstraintContingencyGenerationShiftUpperBound = (
            Constraint(model.S,model.I,model.T, rule=contingency_generation_shift_upper_bound))

        def contingency_generation_upper_bound(model, s, i, t):
            if model.Se !=0:
                return (
                    model.ContingencyGeneration[s,i,t] <=
                    (model.Generation[i,t] + model.ReserveHoldUp[i,t]) *
                    model.ContingencyStatusGenerator[i,s]
                    )
            else:
                return Constraint.Feasible
        model.ConstraintContingencyGenerationUpperBound = (
            Constraint(model.S,model.I,model.T, rule=contingency_generation_upper_bound))

        def contingency_generation_lower_bound(model, s, i, t):
            if model.Se !=0:
                return (
                    model.ContingencyGeneration[s,i,t] >=
                    (model.Generation[i,t] - model.ReserveHoldDown[i,t]) *
                    model.ContingencyStatusGenerator[i,s]
                    )
            else:
                return Constraint.Feasible
        model.ConstraintContingencyGenerationLowerBound = (
            Constraint(model.S,model.I,model.T, rule=contingency_generation_lower_bound))

        def contingency_flow_upper_bound(model, s, l, t):
            if model.Se !=0:
                if model.LInstalled[l]==1:
                    return (
                        model.ContingencyFlow[s,l,t] <=
                        model.MaxFlow[l] *
                        model.ContingencyStatusLine[l,s]
                        )
                if model.LInstalled[l]==0:
                    return (
                        model.ContingencyFlow[s,l,t] <=
                        model.MaxFlow[l] *
                        model.ContingencyStatusLine[l,s] *
                        model.BinInvestment[l]
                        )
            else:
                return Constraint.Feasible
        model.ConstraintContingencyFlowUpperBound = (
            Constraint(model.S,model.L,model.T, rule=contingency_flow_upper_bound))

        def contingency_flow_lower_bound(model, s, l, t):
            if model.Se !=0:
                if model.LInstalled[l]==1:
                    return (
                        model.ContingencyFlow[s,l,t] >=
                        - model.MaxFlow[l] *
                        model.ContingencyStatusLine[l,s]
                        )
                if model.LInstalled[l]==0:
                    return (
                        model.ContingencyFlow[s,l,t] >=
                        - model.MaxFlow[l] *
                        model.ContingencyStatusLine[l,s] *
                        model.BinInvestment[l]
                        )
            else:
                return Constraint.Feasible
        model.ConstraintContingencyFlowLowerBound = (
            Constraint(model.S,model.L,model.T, rule=contingency_flow_lower_bound))

        def contingency_theta_upper_bound(model, s, l, t):
            if model.Se !=0:
                if model.LInstalled[l]==1:
                    return (
                        model.ContingencyFlow[s,l,t] - (
                        model.ContingencyTheta[s,model.LTo[l],t] -
                        model.ContingencyTheta[s,model.LFrom[l],t]
                        ) / model.Reactance[l] <=
                        self.settings['BigM'] * (1 - model.ContingencyStatusLine[l,s])
                        )
                if model.LInstalled[l]==0:
                    return (
                        model.ContingencyFlow[s,l,t] - (
                        model.ContingencyTheta[s,model.LTo[l],t] -
                        model.ContingencyTheta[s,model.LFrom[l],t]
                        ) / model.Reactance[l] <=
                        self.settings['BigM'] * (1 -
                        model.BinInvestment[l] * model.ContingencyStatusLine[l,s])
                        )
            else:
                return Constraint.Feasible
        model.ConstraintContingencyThetaUpperBound = (
            Constraint(model.S,model.L,model.T, rule=contingency_theta_upper_bound))

        def contingency_theta_lower_bound(model, s, l, t):
            if model.Se !=0:
                if model.LInstalled[l]==1:
                    return (
                        model.ContingencyFlow[s,l,t] - (
                        model.ContingencyTheta[s,model.LTo[l],t] -
                        model.ContingencyTheta[s,model.LFrom[l],t]
                        ) / model.Reactance[l] >=
                        - self.settings['BigM'] * (1 - model.ContingencyStatusLine[l,s])
                        )
                if model.LInstalled[l]==0:
                    return (
                        model.ContingencyFlow[s,l,t] - (
                        model.ContingencyTheta[s,model.LTo[l],t] -
                        model.ContingencyTheta[s,model.LFrom[l],t]
                        ) / model.Reactance[l] >=
                        - self.settings['BigM'] * (1 -
                        model.BinInvestment[l] * model.ContingencyStatusLine[l,s])
                        )
            else:
                return Constraint.Feasible
        model.ConstraintContingencyThetaLowerBound = (
            Constraint(model.S,model.L,model.T, rule=contingency_theta_lower_bound))

        model.dual = Suffix(direction=Suffix.IMPORT)

        self.opt_models['investment_operation_risk'] = model


if __name__ == '__main__':
    print('Testing...')
    from system_models.test_small_system import TestSmallSystem
    from system_models.test_small_system_2_gen import TestSmallSystem2Gen
    from system_models.test_small_system_3 import TestSmallSystem3
    from system_models.test_small_system_4 import TestSmallSystem4
    from system_models.test_small_system_2_gen_no_failures import TestSmallSystem2GenNoFailures
    from system_models.test_small_system_no_failures import TestSmallSystemNoFailures
    from system_models.rts_24ieee_1 import RTS24IEEE1
    # self2 = ClassicFormulation()
    self = ClassicFormulation()
    self.execute(datasource=RTS24IEEE1())
# RTS24IEEE1().data
