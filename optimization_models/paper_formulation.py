from __future__ import division
from pyomo.environ import *
import time
import multiprocessing
from pyomo.opt.parallel.manager import solve_all_instances

from optimization_models.model_base import ModelBase

from clusterization_model.clusters import create_cluster
from utils import parse_data, filter_values, drop_values, index_max, index_min
from utils import update_data, reset_index, map_solve

class PaperFormulation(ModelBase):
    '''
    This is the proposed decomposition of the monolithic stochastic security
    constraint planning model.
    '''
    def __init__(self, BigM = 10000, stout = True, varout = False, clusternum = 1,
                 clustermod = 'log'):
        '''
        --
        BigM: (float) parameter to linealize constraints, should be at least
        the max value of the LHS (left hand size) value of constraint.
        stout: (boolean) Indicates if algorithm steps will be printed on
        console.
        clusternum: (integer) This parameter defines how much clusters of
        contingencies should be made while executing this formulation.
        clustermod: (log/kmeans) This parameter defines how should clusters be
        made. We recommend log.
        '''
        super().__init__()
        self.stout = stout
        self.varout = varout
        self.settings = {
            'BigM':BigM,
            'clusternum':clusternum,
            'clustermod':clustermod,
            'model_name': 'PaperFormulation',
            'data_name': None,
        }
        self.data = None
        self.opt_models = {
            'investment_operation': None,
            'risk': None,
            'risk_relaxed': None,
        }
        self._last_instance = {
            'investment_operation': None,
            'risk': {},
            'risk_relaxed': {},
            'risk_worst_contingency': {},
        }
        self.cluster_items = {} # DIM (C,T)
        self.cluster_centroids = {} # DIM (C,T)
        self.cluster_items_remaining = {}
        self.worst_contingencies = {} # DIM (K,S,T) 1 ON 0 OFF
        #################################################
        self._initialize_models()

    def _initialize_models(self):
        self._investment_operation_model()
        self._risk_model()
        self._risk_relaxed_model()

    def execute(self,datasource,solver='gurobi'):
        '''
        Methodology application (Optimization), can be an algorithm
        with multiple optimization models involved.
        --
        data: dictionary defined on system_models/
        solver: optimization solver
        '''
        self._init_data(datasource)
        self._init_clusters()
        self.solution['time'] = {}
        self.solution['time']['investment_operation'] = {}
        self.solution['time']['risk'] = {}
        self.solution['time']['risk_relaxed'] = {}
        self.solution['time']['total'] = None

        time_i_total = time.time()
        for i in range(10000):
            print('Iteration: ',i)
            print(' | Investment and operation')

            self._execute_investment_operation(solver)

            print(' | Risk')
            self._execute_risk(solver)

            self._find_worst_contingencies()

            self._execute_risk_relaxed(solver)

            self._update_status()

            if self.solution['status'] == 'optimal':
                time_f_total = time.time()
                self.solution['time']['total'] = time_f_total - time_i_total
                self.save_solution()
                break

            self._update_data()

        print('Ready')
        print(' | Status:',self.solution['status'])
        print(' | Gap (%):',self.solution['gap'])

    def _init_data(self,datasource):
        '''
        Parse data for first iteration, with 0 Benders Cuts.
        '''
        self.settings['data_name'] = datasource.__class__.__name__
        ini_data = datasource.data
        self.data = datasource.data
        Te = ini_data[None]['Te'][None]
        self.data[None]['Ke'] = parse_data(0)

    def _init_clusters(self):
        '''
        Initialize clusters with probability set for each sample t.
        '''
        Te = self.data[None]['Te'][None]
        Se = self.data[None]['Se'][None]
        Prob = self.data[None]['ProbabilityOccurrence']

        cluster_items = {}
        cluster_centroids = {}
        for t in range(1, Te+1):
            Prob_at_t = [Prob[(s,t)] for s in range(1, Se+1)]
            centroids,items = create_cluster(
                data = Prob_at_t,
                clusternum = self.settings['clusternum'],
                function=self.settings['clustermod']
            )
            for c in range(self.settings['clusternum']):
                cluster_items[(c+1,t)] = [val+1 for val in items[c]]
                cluster_centroids[(c+1,t)] = centroids[c]
        self.cluster_items = cluster_items
        self.cluster_centroids = cluster_centroids
        self.cluster_items_remaining = {idx:val for idx,val in cluster_items.items()}

    def _update_data(self):
        '''
        Worst contingencies for each iteration imply a Benders Cut on
        Investment and Operation module. Those Benders Cuts are updated
        here, to bring more information at next iteration.
        '''
        k = self.data[None]['Ke'][None] + 1
        self.data[None]['Ke'] = parse_data(k)

        Te = self.data[None]['Te'][None]
        Se = self.data[None]['Se'][None]
        for t in range(1, Te+1):
            for s in range(1,Se+1):
                worst_contingencies = [
                    idx[1]
                    for idx in self.worst_contingencies.keys()
                    if idx[2]==t]
                if s in worst_contingencies:
                    risk_instance = self._last_instance['risk_relaxed']
                    update_data(
                        self.data[None], 'KImpact',
                        value(risk_instance[t,s].Obj),
                        {0:k,1:t,2:s})
                    update_data(
                        self.data[None], 'dualGenerationLower',
                        risk_instance[t,s].dualGenerationLower.extract_values(),
                        {0:k,2:t,3:s})
                    update_data(
                        self.data[None], 'dualGenerationUpper',
                        risk_instance[t,s].dualGenerationUpper.extract_values(),
                        {0:k,2:t,3:s})
                    update_data(
                        self.data[None], 'dualGenerationShift',
                        risk_instance[t,s].dualGenerationShift.extract_values(),
                        {0:k,2:t,3:s})
                    update_data(
                        self.data[None], 'dualThetaLower',
                        risk_instance[t,s].dualThetaLower.extract_values(),
                        {0:k,2:t,3:s})
                    update_data(
                        self.data[None], 'dualThetaUpper',
                        risk_instance[t,s].dualThetaUpper.extract_values(),
                        {0:k,2:t,3:s})
                    update_data(
                        self.data[None], 'dualFlowLower',
                        risk_instance[t,s].dualFlowLower.extract_values(),
                        {0:k,2:t,3:s})
                    update_data(
                        self.data[None], 'dualFlowUpper',
                        risk_instance[t,s].dualFlowUpper.extract_values(),
                        {0:k,2:t,3:s})

        update_data(
            self.data[None], 'KGeneration',
            self._last_instance[
                'investment_operation'].Generation.extract_values(),
            {0:k})
        update_data(
            self.data[None], 'KBinInvestment',
            self._last_instance[
                'investment_operation'].BinInvestment.extract_values(),
            {0:k})
        update_data(
            self.data[None], 'KReserveHoldUp',
            self._last_instance[
                'investment_operation'].ReserveHoldUp.extract_values(),
            {0:k})
        update_data(
            self.data[None], 'KReserveHoldDown',
            self._last_instance[
                'investment_operation'].ReserveHoldDown.extract_values(),
            {0:k})

    def _update_status(self):
        last_solution = self._last_instance['investment_operation']
        last_risk = {idx:value(sol.Obj)
            for idx,sol in self._last_instance['risk'].items()}
        upper_bound = value(
            sum(sum(
            last_solution.SampleDuration[t] * (
            last_solution.CostVariable[i] *
            last_solution.Generation[i,t] +
            last_solution.CostReserveHoldUp[i] *
            last_solution.ReserveHoldUp[i,t] +
            last_solution.CostReserveHoldDown[i] *
            last_solution.ReserveHoldDown[i,t])
            for i in last_solution.I)
            for t in last_solution.T) +
            sum(
            last_solution.CostTransmissionInvestment[l] *
            last_solution.BinInvestment[l]
            for l in last_solution.L
            if last_solution.LInstalled[l]==0)
            ) + value(
                sum(sum(
                last_solution.SampleDuration[t] *
                last_solution.ProbabilityOccurrence[s,t] *
                (last_risk[t,(self._cluster_of_contengency(s,t))]
                if (t,(self._cluster_of_contengency(s,t)))
                in self._last_instance['risk']
                else self._last_instance['risk_relaxed'][t,s].Obj)
                for t in last_solution.T)
                for s in last_solution.S)
            )
        # ( for self.worst_contingencies[()])
        actual_solution = self.solution['instance']
        lower_bound_candidate = (value(last_solution.Obj))
        lower_bound = (lower_bound_candidate
                      if actual_solution is None
                      else value(actual_solution.Obj))
        best_iteration = False
        if lower_bound <= lower_bound_candidate:
            best_iteration = True
        if best_iteration:
            lower_bound = lower_bound_candidate
            self.solution['instance'] = (self._last_instance[
                              'investment_operation'])
            gap = 100*(upper_bound-lower_bound)/(lower_bound)
            self.solution['gap'] = gap
            self.solution['status'] = 'unfinished'
            self.save_solution()

        print(' | Status')
        print(' | | Gap (%): ',self.solution['gap'])

        if self.solution['gap'] <= 1: # %
            self.solution['status'] = 'optimal'

    def _cluster_of_contengency(self,contingency,ttime):
        c=None
        for idx,items in self.cluster_items.items():
            if idx[1]==ttime:
                if contingency in items:
                    c = idx[0]
                    break
        return c

    def _find_worst_contingencies(self):
        '''
        Find the outage, for each sample t, with the worst contingency
        over all solved clusters on risk module. Risk of each cluster is
        adjusted (or fixed) with the real probability of the found
        contingency.
        '''
        # Find worst contingency for each cluster (¿faster?)
        # Te = self.data[None]['Te'][None]
        # Ke = self.data[None]['Ke'][None] + 1
        # for t in range(1, Te+1):
        #     worst_contingencies = {}
        #     for c in range(1,self.settings['clusternum']+1):
        #         BinContingencies = (self._last_instance[
        #             'risk'][t,c].BinContingencies.extract_values())
        #         worst_contingency_at_cluster = index_min(BinContingencies)
        #         if worst_contingency_at_cluster > 0:
        #             real_contingency = (
        #                 self.cluster_items[(c,t)][
        #                 worst_contingency_at_cluster-1])
        #             self.worst_contingencies[Ke,real_contingency,t] = 1

        Te = self.data[None]['Te'][None]
        Ke = self.data[None]['Ke'][None] + 1
        for t in range(1, Te+1):
            worst_contingencies = {}
            for c in range(1,self.settings['clusternum']+1):
                if len(self.cluster_items_remaining[(c,t)]) == 0:
                    continue
                BinContingencies = (self._last_instance[
                    'risk'][t,c].BinContingencies.extract_values())
                worst_contingency_at_cluster = index_min(BinContingencies)
                if worst_contingency_at_cluster > 0:
                    real_contingency = (
                        self.cluster_items_remaining[(c,t)][
                        worst_contingency_at_cluster-1])
                    impact = value(self._last_instance[
                        'risk'][t,c].Obj)
                    probability = (self.data[None]['ProbabilityOccurrence'][
                        real_contingency,
                        t])
                    adjusted_risk = impact * probability
                    if adjusted_risk > 0:
                        worst_contingencies[real_contingency
                            ] = adjusted_risk
            worst_contingency = index_max(worst_contingencies)
            if worst_contingency > 0:
                self.worst_contingencies[Ke,worst_contingency,t] = 1
                c = self._cluster_of_contengency(worst_contingency,t)
                self.cluster_items_remaining[(c,t)] = [x
                          for x in self.cluster_items_remaining[(c,t)]
                          if x != worst_contingency]

    def _execute_investment_operation(self,solver):
        self._last_instance['investment_operation'] = None
        instance = (
            self.opt_models['investment_operation'].create_instance(
                data=self.data))
        # MILP Solve
        opt = SolverFactory('gurobi')
        opt.options['NumericFocus'] = 3
        opt.options['Quad'] = 1
        time_i = time.time()
        status = opt.solve(instance)
        time_f = time.time()
        assert status.solver.termination_condition == 'optimal'

        # # MILP Solve
        # instance.BinCommit.fix()
        # for l in instance.L:
        #     if instance.LInstalled[l]==0:
        #         instance.BinInvestment[l].fix()
        # status = opt.solve(instance)
        # assert status.solver.termination_condition == 'optimal'

        self._last_instance['investment_operation'] = instance
        k = self.data[None]['Ke'][None] + 1
        self.solution['time']['investment_operation'][k] = time_f-time_i
        # Temporal results summary
        if self.stout:
            print(' | | Objective: ',value(instance.Obj))
            print(' | | Operation: ',sum(sum(
                instance.SampleDuration[t] *
                (instance.CostVariable[i] * instance.Generation[i,t].value +
                instance.CostReserveHoldUp[i] * instance.ReserveHoldUp[i,t].value +
                instance.CostReserveHoldDown[i] * instance.ReserveHoldDown[i,t].value) +
                0 for i in instance.I) for t in instance.T) )
            print(' | | Risk: ',sum(sum(
                instance.SampleDuration[t] *
                instance.ProbabilityOccurrence[s,t] *
                instance.RiskCut[s,t].value +
                0 for t in instance.T) for s in instance.S if instance.Se!=0))
            print(' | | Investment: ',sum(instance.CostTransmissionInvestment[l] * instance.BinInvestment[l].value +
                0 for l in instance.L if instance.LInstalled[l]==0) )
            print(' | | Investment: ',{i:v
                for i,v in instance.BinInvestment.extract_values().items()
                if v is not None and v >0.95})
        if self.varout:
            print(' | | Generation:')
            for i in instance.I:
                print(' | | | Unit {}'.format(i),['{0:3.0f}'.format(instance.Generation[i,t].value) for t in instance.T])
            print(' | | Reserves Up:')
            for i in instance.I:
                print(' | | | Unit {}'.format(i),['{0:3.0f}'.format(instance.ReserveHoldUp[i,t].value) for t in instance.T])
            print(' | | Reserves Down:')
            for i in instance.I:
                print(' | | | Unit {}'.format(i),['{0:3.0f}'.format(instance.ReserveHoldDown[i,t].value) for t in instance.T])

    def _execute_risk(self,solver):
        self._last_instance['risk'] = {}
        Te = self.data[None]['Te'][None]
        map_inputs = {'instance':[],'index':[]}
        for t in range(1,Te+1):
            for c in range(1,self.settings['clusternum']+1):
                if len(self.cluster_items_remaining[(c,t)]) == 0:
                    continue
                map_inputs['index'] += [(t,c)]
                data = self._risk_input_data(t,c)
                instance = (
                    self.opt_models['risk'].create_instance(
                        data=data))
                map_inputs['instance'] += [instance]

        solver_manager = SolverManagerFactory('serial')
        time_i = time.time()
        solve_all_instances(solver_manager,solver,map_inputs['instance'])
        time_f = time.time()
        # pool = multiprocessing.Pool(4)
        # results = list(pool.map(
        #     map_solve,
        #     map_inputs['instance']))
        k = self.data[None]['Ke'][None] + 1
        self.solution['time']['risk'][k] = time_f-time_i
        for i,idx in enumerate(map_inputs['index']):
            print(' | T: ',idx[0], ', C: ',idx[1])
            instance = map_inputs['instance'][i]
            # status = instance.status
            # instance
            # assert status.termination_condition == 'optimal'

            self._last_instance['risk'][idx] = instance
            # Temporal results summary
            if self.stout:
                print(' | | | Objective: ',value(instance.Obj))
                print(' | | | Contingencies: ',instance.BinContingencies.extract_values())

    def _map_instances(self,instance):
        opt = SolverFactory('gurobi')
        opt.options['NumericFocus'] = 3
        opt.options['Quad'] = 1
        opt.options['Presolve'] = 2
        status = opt.solve(instance)
        return instance, status

    def _execute_risk_relaxed(self,solver):
        self._last_instance['risk_relaxed'] = {}
        Te = self.data[None]['Te'][None]
        map_inputs = {'instance':[],'index':[]}
        for t in range(1,Te+1):
            worst_contingencies = [
                idx[1]
                for idx in self.worst_contingencies.keys()
                if idx[2]==t]
            for s in worst_contingencies:
                map_inputs['index'] += [(t,s)]
                data = self._risk_relaxed_input_data(t,s)
                instance = (
                    self.opt_models['risk_relaxed'].create_instance(
                        data=data))
                map_inputs['instance'] += [instance]

        solver_manager = SolverManagerFactory('serial')
        time_i = time.time()
        solve_all_instances(solver_manager,solver,map_inputs['instance'])
        time_f = time.time()

        k = self.data[None]['Ke'][None] + 1
        self.solution['time']['risk_relaxed'][k] = time_f-time_i
        for i,idx in enumerate(map_inputs['index']):
            print(' | T: ',idx[0], ', S: ',idx[1])
            instance = map_inputs['instance'][i]
            # status = instance.status
            # instance
            # assert status.termination_condition == 'optimal'

            self._last_instance['risk_relaxed'][idx] = instance
            # Temporal results summary
            if self.stout:
                print(' | | Objective: ',value(instance.Obj))

    def _risk_input_data(self,t, c):
        '''
        This module generate data for Risk model according with last solution
        of Operation and Investment model.
        --
        t: time sample
        c: cluster number
        '''
        instance = self._last_instance['investment_operation']

        data = {}
        data['Ie'] = instance.Ie.extract_values()
        data['Le'] = instance.Le.extract_values()
        data['Ne'] = instance.Ne.extract_values()
        data['Ce'] = parse_data(
                     len(self.cluster_items_remaining[(c,t)]))

        data['LTo'] = instance.LTo.extract_values()
        data['LFrom'] = instance.LFrom.extract_values()
        data['LInstalled'] = instance.LInstalled.extract_values()
        data['INode'] = instance.INode.extract_values()

        data['CostGenerationShift'] = (
            instance.CostGenerationShift.extract_values())
        data['MaxFlow'] = instance.MaxFlow.extract_values()
        data['Reactance'] = instance.Reactance.extract_values()
        data['CostLostLoad'] = instance.CostLostLoad.extract_values()

        data['Load'] = (
            drop_values(instance.Load.extract_values(),{2:[t]}))
        data['Generation'] = (
            drop_values(instance.Generation.extract_values(),{2:[t]}))
        data['ReserveHoldUp'] = (
            drop_values(instance.ReserveHoldUp.extract_values(),{2:[t]}))
        data['ReserveHoldDown'] = (
            drop_values(instance.ReserveHoldDown.extract_values(),{2:[t]}))
        data['BinInvestment'] = instance.BinInvestment.extract_values()

        data['ProbabilityOccurrence'] = (
            parse_data(self.cluster_centroids[(c,t)]))

        data['ContingencyStatusGenerator'] = reset_index(
            filter_values(
            instance.ContingencyStatusGenerator.extract_values(),
            {2:self.cluster_items_remaining[(c,t)]}),
            [1])
        data['ContingencyStatusLine'] = reset_index(
            filter_values(
            instance.ContingencyStatusLine.extract_values(),
            {2:self.cluster_items_remaining[(c,t)]}),
            [1])

        return {None:data}

    def _risk_relaxed_input_data(self,t, s):
        '''
        This module generate data for Risk model according with last solution
        of Operation and Investment model.
        --
        t: time sample
        s: outage number
        '''
        instance = self._last_instance['investment_operation']

        data = {}
        data['Ie'] = instance.Ie.extract_values()
        data['Le'] = instance.Le.extract_values()
        data['Ne'] = instance.Ne.extract_values()

        data['LTo'] = instance.LTo.extract_values()
        data['LFrom'] = instance.LFrom.extract_values()
        data['LInstalled'] = instance.LInstalled.extract_values()
        data['INode'] = instance.INode.extract_values()

        data['CostGenerationShift'] = (
            instance.CostGenerationShift.extract_values())
        data['MaxFlow'] = instance.MaxFlow.extract_values()
        data['Reactance'] = instance.Reactance.extract_values()
        data['CostLostLoad'] = instance.CostLostLoad.extract_values()

        data['Load'] = (
            drop_values(instance.Load.extract_values(),{2:[t]}))
        data['Generation'] = (
            drop_values(instance.Generation.extract_values(),{2:[t]}))
        data['ReserveHoldUp'] = (
            drop_values(instance.ReserveHoldUp.extract_values(),{2:[t]}))
        data['ReserveHoldDown'] = (
            drop_values(instance.ReserveHoldDown.extract_values(),{2:[t]}))
        data['BinInvestment'] = instance.BinInvestment.extract_values()

        data['ProbabilityOccurrence'] = (
            parse_data(instance.ProbabilityOccurrence.extract_values()[(s,t)]))

        data['ContingencyStatusGenerator'] = drop_values(
            instance.ContingencyStatusGenerator.extract_values(),
            {2:[s]})

        data['ContingencyStatusLine'] = drop_values(
            instance.ContingencyStatusLine.extract_values(),
            {2:[s]})

        return {None:data}

    def _risk_worst_contingency_input_data(self,t, s):
        '''
        This module generate data for Risk model according with last solution
        of Operation and Investment model.
        --
        t: time sample
        s: outage number
        '''
        instance = self._last_instance['investment_operation']

        data = {}
        data['Ie'] = instance.Ie.extract_values()
        data['Le'] = instance.Le.extract_values()
        data['Ne'] = instance.Ne.extract_values()
        data['Ce'] = {None:1}

        data['LTo'] = instance.LTo.extract_values()
        data['LFrom'] = instance.LFrom.extract_values()
        data['LInstalled'] = instance.LInstalled.extract_values()
        data['INode'] = instance.INode.extract_values()

        data['CostGenerationShift'] = (
            instance.CostGenerationShift.extract_values())
        data['MaxFlow'] = instance.MaxFlow.extract_values()
        data['Reactance'] = instance.Reactance.extract_values()
        data['CostLostLoad'] = instance.CostLostLoad.extract_values()

        data['Load'] = (
            drop_values(instance.Load.extract_values(),{2:[t]}))
        data['Generation'] = (
            drop_values(instance.Generation.extract_values(),{2:[t]}))
        data['ReserveHoldUp'] = (
            drop_values(instance.ReserveHoldUp.extract_values(),{2:[t]}))
        data['ReserveHoldDown'] = (
            drop_values(instance.ReserveHoldDown.extract_values(),{2:[t]}))
        data['BinInvestment'] = instance.BinInvestment.extract_values()

        data['ProbabilityOccurrence'] = (
            parse_data(self.data[None]['ProbabilityOccurrence'][s,t]))

        data['ContingencyStatusGenerator'] = reset_index(
            filter_values(
            instance.ContingencyStatusGenerator.extract_values(),
            {2:[s]}),
            [1])
        data['ContingencyStatusLine'] = reset_index(
            filter_values(
            instance.ContingencyStatusLine.extract_values(),
            {2:[s]}),
            [1])

        return {None:data}

    def _investment_operation_model(self):
        '''
        Investment and Operation model definition. This model co-optimize
        Investment costs and Operation costs, while Risk is approximated
        through Benders Cuts.
        '''
        model = AbstractModel()

        ## Number of elements per Set
        model.Ie = Param(within=NonNegativeIntegers)
        model.Te = Param(within=NonNegativeIntegers)
        model.Le = Param(within=NonNegativeIntegers)
        model.Se = Param(within=NonNegativeIntegers)
        model.Ne = Param(within=NonNegativeIntegers)
        model.Ke = Param(within=NonNegativeIntegers)

        ## Sets
        model.I = RangeSet(1, model.Ie)
        model.T = RangeSet(1, model.Te)
        model.S = RangeSet(1, model.Se)
        model.L = RangeSet(1, model.Le)
        model.N = RangeSet(1, model.Ne)
        model.K = RangeSet(1, model.Ke)
        ## Subsets
        model.LTo = Param(model.L)
        model.LFrom = Param(model.L)
        model.LInstalled = Param(model.L)
        model.INode = Param(model.I)

        ## System Parameters
        # Generators
        model.CostVariable = Param(model.I)
        model.CostReserveHoldUp = Param(model.I)
        model.CostReserveHoldDown = Param(model.I)
        model.MaxReserveHoldUp = Param(model.I)
        model.MaxReserveHoldDown = Param(model.I)
        model.MaxCapacity = Param(model.I)
        model.MinStableLevel = Param(model.I)
        model.CostGenerationShift = Param() #Curtailment mejor, no se
        # Lines
        model.MaxFlow = Param(model.L)
        model.Reactance = Param(model.L)
        model.CostTransmissionInvestment = Param(model.L)
        # Contingences
        model.ProbabilityOccurrence = Param(model.S,model.T)
        model.ContingencyStatusGenerator = Param(model.I,model.S)
        model.ContingencyStatusLine = Param(model.L,model.S)
        # Load
        model.CostLostLoad = Param()
        model.Load = Param(model.N,model.T)
        model.SampleDuration = Param(model.T)
        # Benders Cuts
        model.KImpact = Param(model.K,model.T,model.S)
        model.KGeneration = Param(model.K,model.I,model.T)
        model.KBinInvestment = Param(model.K,model.L, within=Any)
        model.KReserveHoldUp = Param(model.K,model.I,model.T)
        model.KReserveHoldDown = Param(model.K,model.I,model.T)

        model.dualGenerationLower = Param(model.K,model.I,model.T,model.S)
        model.dualGenerationUpper = Param(model.K,model.I,model.T,model.S)
        model.dualGenerationShift = Param(model.K,model.I,model.T,model.S)
        model.dualThetaLower = Param(model.K,model.L,model.T,model.S)
        model.dualThetaUpper = Param(model.K,model.L,model.T,model.S)
        model.dualFlowLower = Param(model.K,model.L,model.T,model.S)
        model.dualFlowUpper = Param(model.K,model.L,model.T,model.S)


        ## Variables
        # Generators
        model.Generation = Var(model.I,model.T, domain=NonNegativeReals)
        model.BinCommit = Var(model.I,model.T, domain=Binary)
        model.ReserveHoldUp = Var(model.I,model.T, domain=NonNegativeReals)
        model.ReserveHoldDown = Var(model.I,model.T, domain=NonNegativeReals)
        # model.ContingencyGeneration = Var(model.S,model.I,model.T, domain=NonNegativeReals)
        # model.ContingencyReserveHoldUp = Var(model.S,model.I,model.T, domain=NonNegativeReals)
        # model.ContingencyReserveHoldDown = Var(model.S,model.I,model.T, domain=NonNegativeReals)
        # model.GenerationShift = Var(model.S,model.I,model.T, domain=NonNegativeReals)
        # Lines
        model.Flow = Var(model.L,model.T)
        model.Theta = Var(model.N,model.T)
        model.BinInvestment = Var(model.L, domain=Binary)
        # model.ContingencyFlow = Var(model.S,model.L,model.T)
        # model.ContingencyTheta = Var(model.S,model.N,model.T)
        # Load
        # model.LostLoad = Var(model.S,model.N,model.T, domain=NonNegativeReals)
        # BenderCut
        model.RiskCut = Var(model.S,model.T, domain=NonNegativeReals)

        ## Objective Function
        def fo_handler(model):
            return (
                sum(sum(
                    model.SampleDuration[t] *
                    (model.CostVariable[i] * model.Generation[i,t] +
                    model.CostReserveHoldUp[i] * model.ReserveHoldUp[i,t] +
                    model.CostReserveHoldDown[i] * model.ReserveHoldDown[i,t]) +
                    0 for i in model.I) for t in model.T) +
                sum(model.CostTransmissionInvestment[l] * model.BinInvestment[l] +
                    0 for l in model.L if model.LInstalled[l]==0) +
                sum(sum(
                    model.SampleDuration[t] *
                    model.ProbabilityOccurrence[s,t] *
                    model.RiskCut[s,t] +
                    0 for t in model.T) for s in model.S if model.Se!=0)
                )
        model.Obj = Objective(rule=fo_handler, sense=minimize)

        ## Constraints
        # Without contingencies constraints
        def balance_generation_load(model, b, t):
            return (
                sum(model.Generation[i,t] for i in model.I if model.INode[i]==b) ==
                sum(model.Flow[l,t] for l in model.L if model.LFrom[l]==b) -
                sum(model.Flow[l,t] for l in model.L if model.LTo[l]==b) +
                model.Load[b,t]
                )
        model.ConstraintBalance = Constraint(model.N,model.T, rule=balance_generation_load)

        def reserve_hold_up(model, i, t):
            return (
                model.ReserveHoldUp[i,t] <=
                model.MaxReserveHoldUp[i] * model.BinCommit[i,t]
                )
        model.ConstraintReserveHoldUp = Constraint(model.I,model.T, rule=reserve_hold_up)

        def reserve_hold_down(model, i, t):
            return (
                model.ReserveHoldDown[i,t] <=
                model.MaxReserveHoldDown[i] * model.BinCommit[i,t]
                )
        model.ConstraintReserveHoldDown = Constraint(model.I,model.T, rule=reserve_hold_down)

        def generation_upper_bound(model, i, t):
            return (
                model.Generation[i,t] + model.ReserveHoldUp[i,t] <=
                model.MaxCapacity[i] * model.BinCommit[i,t]
                )
        model.ConstraintGenerationUpperBound = Constraint(model.I,model.T, rule=generation_upper_bound)

        def generation_lower_bound(model, i, t):
            return (
                model.Generation[i,t] - model.ReserveHoldDown[i,t] >=
                model.MinStableLevel[i] * model.BinCommit[i,t]
                )
        model.ConstraintGenerationLowerBound = Constraint(model.I,model.T, rule=generation_lower_bound)

        def flow_upper_bound(model, l, t):
            if model.LInstalled[l]==1:
                return (
                    model.Flow[l,t] <= model.MaxFlow[l]
                    )
            elif model.LInstalled[l]==0:
                return (
                    model.Flow[l,t] <= model.MaxFlow[l] * model.BinInvestment[l]
                    )
        model.ConstraintFlowUpperBound = Constraint(model.L,model.T, rule=flow_upper_bound)

        def flow_lower_bound(model, l, t):
            if model.LInstalled[l]==1:
                return (
                    model.Flow[l,t] >= - model.MaxFlow[l]
                    )
            elif model.LInstalled[l]==0:
                return (
                    model.Flow[l,t] >= - model.MaxFlow[l] * model.BinInvestment[l]
                    )
        model.ConstraintFlowLowerBound = Constraint(model.L,model.T, rule=flow_lower_bound)

        def theta_upper_bound(model, l, t):
            if model.LInstalled[l]==1:
                return (
                    model.Flow[l,t] <= (
                        model.Theta[model.LTo[l],t] -
                        model.Theta[model.LFrom[l],t]
                        ) / model.Reactance[l]
                    )
            elif model.LInstalled[l]==0:
                return (
                    model.Flow[l,t] - (
                        model.Theta[model.LTo[l],t] -
                        model.Theta[model.LFrom[l],t]
                        ) / model.Reactance[l] <=
                        self.settings['BigM'] * (1-model.BinInvestment[l])
                    )
        model.ConstraintThetaUpperBound = Constraint(model.L,model.T, rule=theta_upper_bound)

        def theta_lower_bound(model, l, t):
            if model.LInstalled[l]==1:
                return (
                    model.Flow[l,t] >= (
                        model.Theta[model.LTo[l],t] -
                        model.Theta[model.LFrom[l],t]
                        ) / model.Reactance[l]
                    )
            elif model.LInstalled[l]==0:
                return (
                    model.Flow[l,t] - (
                        model.Theta[model.LTo[l],t] -
                        model.Theta[model.LFrom[l],t]
                        ) / model.Reactance[l] >=
                        - self.settings['BigM'] * (1-model.BinInvestment[l])
                    )
        model.ConstraintThetaLowerBound = (
            Constraint(model.L,model.T, rule=theta_lower_bound))

        # Contingency constraints
        def benders_cuts(model, k, s, t):
            if (k,t,s) in model.KImpact:
                return (model.RiskCut[s,t] >=
                    model.KImpact[k,t,s] +
                    sum((model.Generation[i,t] - model.KGeneration[k,i,t]) *
                        (model.dualGenerationLower[k,i,t,s]*model.ContingencyStatusGenerator[i,s] -
                        model.dualGenerationShift[k,i,t,s]*model.ContingencyStatusGenerator[i,s] -
                        model.dualGenerationUpper[k,i,t,s]*model.ContingencyStatusGenerator[i,s])
                    for i in model.I) +
                    sum((model.BinInvestment[l]-model.KBinInvestment[k,l]) *
                        (self.settings['BigM'] * (
                            model.dualThetaLower[k,l,t,s]*model.ContingencyStatusLine[l,s] -
                            model.dualThetaUpper[k,l,t,s]*model.ContingencyStatusLine[l,s]) -
                        model.MaxFlow[l] * ( +
                            model.dualFlowLower[k,l,t,s]*model.ContingencyStatusLine[l,s] -
                            model.dualFlowUpper[k,l,t,s]*model.ContingencyStatusLine[l,s]))
                    for l in model.L if model.LInstalled[l]==0) +
                    sum((model.ReserveHoldUp[i,t]-model.KReserveHoldUp[k,i,t]) *
                        (- model.dualGenerationUpper[k,i,t,s]*model.ContingencyStatusGenerator[i,s])
                    for i in model.I) +
                    sum((model.ReserveHoldDown[i,t]-model.KReserveHoldDown[k,i,t]) *
                        (- model.dualGenerationLower[k,i,t,s]*model.ContingencyStatusGenerator[i,s])
                    for i in model.I)
                    )
            else:
                return Constraint.Feasible
        model.ConstraintBendersCuts = (
            Constraint(model.K,model.S,model.T, rule=benders_cuts))
        model.dual = Suffix(direction=Suffix.IMPORT)

        self.opt_models['investment_operation'] = model

    def _risk_model(self):
        '''
        Risk model definition. This model identify worst case contingencies
        given the suboptimal operation determined on previous iteration of
        Investment and Operation Module.
        '''
        model = AbstractModel()

        ## Number of elements per Set
        model.Ie = Param(within=NonNegativeIntegers)
        model.Le = Param(within=NonNegativeIntegers)
        model.Ce = Param(within=NonNegativeIntegers)
        model.Ne = Param(within=NonNegativeIntegers)

        ## Sets
        model.I = RangeSet(1, model.Ie)
        model.L = RangeSet(1, model.Le)
        model.N = RangeSet(1, model.Ne)
        model.C = RangeSet(1, model.Ce)
        ## Subsets
        model.LTo = Param(model.L)
        model.LFrom = Param(model.L)
        model.LInstalled = Param(model.L)
        model.INode = Param(model.I)
        # model.CToS = Param(model.C)


        ## System Parameters
        # Generators
        model.CostGenerationShift = Param() #Curtailment mejor, no se
        model.Generation = Param(model.I)
        model.ReserveHoldUp = Param(model.I)
        model.ReserveHoldDown = Param(model.I)
        # Lines
        model.MaxFlow = Param(model.L)
        model.Reactance = Param(model.L)
        model.BinInvestment = Param(model.L,within=Any)
        # Contingences
        model.ProbabilityOccurrence = Param()
        model.ContingencyStatusGenerator = Param(model.I,model.C)
        model.ContingencyStatusLine = Param(model.L,model.C)
        # Load
        model.CostLostLoad = Param()
        model.Load = Param(model.N)

        ## Variables
        # Contingencies
        model.BinContingencies = Var(model.C, domain=Binary)
        model.BinContingencyStatusGenerator = Var(model.I, domain=Binary)
        model.BinContingencyStatusLine = Var(model.L, domain=Binary)
        # Operation dual variables
        model.dualBalance = Var(model.N)
        model.dualFlowUpper = Var(model.L, domain=NonNegativeReals)
        model.dualFlowLower = Var(model.L, domain=NonNegativeReals)
        model.dualThetaUpper = Var(model.L, domain=NonNegativeReals)
        model.dualThetaLower = Var(model.L, domain=NonNegativeReals)
        model.dualGenerationUpper = Var(model.I, domain=NonNegativeReals)
        model.dualGenerationLower = Var(model.I, domain=NonNegativeReals)
        model.dualLostLoad = Var(model.N, domain=NonNegativeReals)
        model.dualGenerationShift = Var(model.I, domain=NonNegativeReals)
        # Auxiliary variables to linearize
        model.dualLinFlowUpper = Var(model.L, domain=NonNegativeReals)
        model.dualLinFlowLower = Var(model.L, domain=NonNegativeReals)
        model.dualLinThetaUpper = Var(model.L, domain=NonNegativeReals)
        model.dualLinThetaLower = Var(model.L, domain=NonNegativeReals)
        model.dualLinGenerationUpper = Var(model.I, domain=NonNegativeReals)
        model.dualLinGenerationLower = Var(model.I, domain=NonNegativeReals)

        ## Objective Function
        def fo_handler(model):
            return (
                sum(model.Load[b] * (model.dualBalance[b]-model.dualLostLoad[b])
                for b in model.N) -
                sum(model.Generation[i] * model.dualGenerationShift[i]
                for i in model.I) -
                sum((model.Generation[i]+model.ReserveHoldUp[i]) *
                    model.dualLinGenerationUpper[i] -
                    (model.Generation[i]-model.ReserveHoldDown[i]) *
                    model.dualLinGenerationLower[i]
                for i in model.I) -
                sum(model.MaxFlow[l] * model.dualLinFlowUpper[l] +
                    model.MaxFlow[l] * model.dualLinFlowLower[l]
                for l in model.L if model.LInstalled[l]==1) -
                sum(model.MaxFlow[l] * model.BinInvestment[l] *
                    model.dualLinFlowUpper[l] +
                    model.MaxFlow[l] * model.BinInvestment[l] *
                    model.dualLinFlowLower[l]
                for l in model.L if model.LInstalled[l]==0) -
                sum(self.settings['BigM'] * (
                    (model.dualThetaUpper[l] - model.dualLinThetaUpper[l]) +
                    (model.dualThetaLower[l] - model.dualLinThetaLower[l]))
                for l in model.L if model.LInstalled[l]==1) -
                sum(self.settings['BigM'] * (
                    (model.dualThetaUpper[l] - model.dualLinThetaUpper[l] *
                        model.BinInvestment[l]) +
                    (model.dualThetaLower[l] - model.dualLinThetaLower[l] *
                        model.BinInvestment[l]))
                for l in model.L if model.LInstalled[l]==0)
                )
        model.Obj = Objective(rule=fo_handler, sense=maximize)

        ## Constraints
        def worst_contingency(model):
            return (
                sum(model.BinContingencies[c] for c in model.C
                    ) ==
                model.Ce -1
                )
        model.ConstraintWorstContingency = Constraint(rule=worst_contingency)

        def status_contingency(model, c):
            return (
                sum(model.ContingencyStatusLine[l,c] -
                    (2 * model.ContingencyStatusLine[l,c] - 1) *
                    model.BinContingencyStatusLine[l]
                for l in model.L) +
                sum(model.ContingencyStatusGenerator[i,c] -
                    (2 * model.ContingencyStatusGenerator[i,c] - 1) *
                    model.BinContingencyStatusGenerator[i]
                for i in model.I) <= self.settings['BigM'] * model.BinContingencies[c]
                )
        model.ConstraintStatusContingency = (
            Constraint(model.C,rule=status_contingency))

        def dual_generation(model, i):
            return (
                model.dualBalance[model.INode[i]] +
                model.dualGenerationLower[i] - model.dualGenerationUpper[i] <=
                0
                )
        model.ConstraintDualGeneration = (
            Constraint(model.I, rule=dual_generation))

        def dual_flow(model, l):
            return (
                model.dualBalance[model.LTo[l]] -
                model.dualBalance[model.LFrom[l]] +
                model.dualFlowLower[l] - model.dualFlowUpper[l] +
                model.dualThetaLower[l] - model.dualThetaUpper[l] == 0
                )
        model.ConstraintDualFlow = (
            Constraint(model.L, rule=dual_flow))

        def dual_theta(model, b):
            return (
                sum((model.dualThetaUpper[l]-model.dualThetaLower[l]) /
                    model.Reactance[l]
                for l in model.L if model.LTo[l]==b) -
                sum((model.dualThetaUpper[l]-model.dualThetaLower[l]) /
                    model.Reactance[l]
                for l in model.L if model.LFrom[l]==b) ==0
                )
        model.ConstraintDualTheta = (
            Constraint(model.N, rule=dual_theta))

        def dual_lost_load(model, b):
            return (
                model.CostLostLoad - model.dualBalance[b] +
                model.dualLostLoad[b] >= 0
                )
        model.ConstraintDualLostLoad = (
            Constraint(model.N, rule=dual_lost_load))

        def dual_generation_shift(model, i):
            return (
                model.CostGenerationShift + model.dualBalance[model.INode[i]] +
                model.dualGenerationShift[i] >= 0
                )
        model.ConstraintDualGenerationShift = (
            Constraint(model.I, rule=dual_generation_shift))

        def lin_generation_upper_one_upper(model, i):
            return (
                model.dualLinGenerationUpper[i] -
                model.dualGenerationUpper[i] <=
                self.settings['BigM'] * (1 - model.BinContingencyStatusGenerator[i])
                )
        model.ConstraintLinGenerationUpperOneUpper = (
            Constraint(model.I, rule=lin_generation_upper_one_upper))

        def lin_generation_upper_one_lower(model, i):
            return (
                model.dualLinGenerationUpper[i] -
                model.dualGenerationUpper[i] >=
                - self.settings['BigM'] * (1 - model.BinContingencyStatusGenerator[i])
                )
        model.ConstraintLinGenerationUpperOneLower = (
            Constraint(model.I, rule=lin_generation_upper_one_lower))

        def lin_generation_upper_two_upper(model, i):
            return (
                model.dualLinGenerationUpper[i] <=
                self.settings['BigM'] * model.BinContingencyStatusGenerator[i]
                )
        model.ConstraintLinGenerationUpperTwoUpper = (
            Constraint(model.I, rule=lin_generation_upper_two_upper))

        def lin_generation_lower_one_upper(model, i):
            return (
                model.dualLinGenerationLower[i] -
                model.dualGenerationLower[i] <=
                self.settings['BigM'] * (1 - model.BinContingencyStatusGenerator[i])
                )
        model.ConstraintLinGenerationLowerOneUpper = (
            Constraint(model.I, rule=lin_generation_lower_one_upper))

        def lin_generation_lower_one_lower(model, i):
            return (
                model.dualLinGenerationLower[i] -
                model.dualGenerationLower[i] >=
                - self.settings['BigM'] * (1 - model.BinContingencyStatusGenerator[i])
                )
        model.ConstraintLinGenerationLowerOneLower = (
            Constraint(model.I, rule=lin_generation_lower_one_lower))

        def lin_generation_lower_two_upper(model, i):
            return (
                model.dualLinGenerationLower[i] <=
                self.settings['BigM'] * model.BinContingencyStatusGenerator[i]
                )
        model.ConstraintLinGenerationLowerTwoUpper = (
            Constraint(model.I, rule=lin_generation_lower_two_upper))

        def lin_flow_upper_one_upper(model, l):
            return (
                model.dualLinFlowUpper[l] -
                model.dualFlowUpper[l] <=
                self.settings['BigM'] * (1 - model.BinContingencyStatusLine[l])
                )
        model.ConstraintLinFlowUpperOneUpper = (
            Constraint(model.L, rule=lin_flow_upper_one_upper))

        def lin_flow_upper_one_lower(model, l):
            return (
                model.dualLinFlowUpper[l] -
                model.dualFlowUpper[l] >=
                - self.settings['BigM'] * (1 - model.BinContingencyStatusLine[l])
                )
        model.ConstraintLinFlowUpperOneLower = (
            Constraint(model.L, rule=lin_flow_upper_one_lower))

        def lin_flow_upper_two_upper(model, l):
            return (
                model.dualLinFlowUpper[l] <=
                self.settings['BigM'] * model.BinContingencyStatusLine[l]
                )
        model.ConstraintLinFlowUpperTwoUpper = (
            Constraint(model.L, rule=lin_flow_upper_two_upper))

        def lin_flow_lower_one_upper(model, l):
            return (
                model.dualLinFlowLower[l] -
                model.dualFlowLower[l] <=
                self.settings['BigM'] * (1 - model.BinContingencyStatusLine[l])
                )
        model.ConstraintLinFlowLowerOneUpper = (
            Constraint(model.L, rule=lin_flow_lower_one_upper))

        def lin_flow_lower_one_lower(model, l):
            return (
                model.dualLinFlowLower[l] -
                model.dualFlowLower[l] >=
                - self.settings['BigM'] * (1 - model.BinContingencyStatusLine[l])
                )
        model.ConstraintLinFlowLowerOneLower = (
            Constraint(model.L, rule=lin_flow_lower_one_lower))

        def lin_flow_lower_two_upper(model, l):
            return (
                model.dualLinFlowLower[l] <=
                self.settings['BigM'] * model.BinContingencyStatusLine[l]
                )
        model.ConstraintLinFlowLowerTwoUpper = (
            Constraint(model.L, rule=lin_flow_lower_two_upper))

        def lin_theta_upper_one_upper(model, l):
            return (
                model.dualLinThetaUpper[l] -
                model.dualThetaUpper[l] <=
                self.settings['BigM'] * (1 - model.BinContingencyStatusLine[l])
                )
        model.ConstraintLinThetaUpperOneUpper = (
            Constraint(model.L, rule=lin_theta_upper_one_upper))

        def lin_theta_upper_one_lower(model, l):
            return (
                model.dualLinThetaUpper[l] -
                model.dualThetaUpper[l] >=
                - self.settings['BigM'] * (1 - model.BinContingencyStatusLine[l])
                )
        model.ConstraintLinThetaUpperOneLower = (
            Constraint(model.L, rule=lin_theta_upper_one_lower))

        def lin_theta_upper_two_upper(model, l):
            return (
                model.dualLinThetaUpper[l] <=
                self.settings['BigM'] * model.BinContingencyStatusLine[l]
                )
        model.ConstraintLinThetaUpperTwoUpper = (
            Constraint(model.L, rule=lin_theta_upper_two_upper))

        def lin_theta_lower_one_upper(model, l):
            return (
                model.dualLinThetaLower[l] -
                model.dualThetaLower[l] <=
                self.settings['BigM'] * (1 - model.BinContingencyStatusLine[l])
                )
        model.ConstraintLinThetaLowerOneUpper = (
            Constraint(model.L, rule=lin_theta_lower_one_upper))

        def lin_theta_lower_one_lower(model, l):
            return (
                model.dualLinThetaLower[l] -
                model.dualThetaLower[l] >=
                - self.settings['BigM'] * (1 - model.BinContingencyStatusLine[l])
                )
        model.ConstraintLinThetaLowerOneLower = (
            Constraint(model.L, rule=lin_theta_lower_one_lower))

        def lin_theta_lower_two_upper(model, l):
            return (
                model.dualLinThetaLower[l] <=
                self.settings['BigM'] * model.BinContingencyStatusLine[l]
                )
        model.ConstraintLinThetaLowerTwoUpper = (
            Constraint(model.L, rule=lin_theta_lower_two_upper))

        model.dual = Suffix(direction=Suffix.IMPORT)

        self.opt_models['risk'] = model

    def _risk_relaxed_model(self):
        '''
        Risk model definition. This model identify worst case contingencies
        given the suboptimal operation determined on previous iteration of
        Investment and Operation Module.
        '''
        model = AbstractModel()

        ## Number of elements per Set
        model.Ie = Param(within=NonNegativeIntegers)
        model.Le = Param(within=NonNegativeIntegers)
        model.Ne = Param(within=NonNegativeIntegers)

        ## Sets
        model.I = RangeSet(1, model.Ie)
        model.L = RangeSet(1, model.Le)
        model.N = RangeSet(1, model.Ne)
        ## Subsets
        model.LTo = Param(model.L)
        model.LFrom = Param(model.L)
        model.LInstalled = Param(model.L)
        model.INode = Param(model.I)

        ## System Parameters
        # Generators
        model.CostGenerationShift = Param() #Curtailment mejor, no se
        model.Generation = Param(model.I)
        model.ReserveHoldUp = Param(model.I)
        model.ReserveHoldDown = Param(model.I)
        # Lines
        model.MaxFlow = Param(model.L)
        model.Reactance = Param(model.L)
        model.BinInvestment = Param(model.L,within=Any)
        # Contingences
        model.ProbabilityOccurrence = Param()
        model.ContingencyStatusGenerator = Param(model.I)
        model.ContingencyStatusLine = Param(model.L)
        # Load
        model.CostLostLoad = Param()
        model.Load = Param(model.N)

        ## Variables
        # Operation dual variables
        model.dualBalance = Var(model.N)
        model.dualFlowUpper = Var(model.L, domain=NonNegativeReals)
        model.dualFlowLower = Var(model.L, domain=NonNegativeReals)
        model.dualThetaUpper = Var(model.L, domain=NonNegativeReals)
        model.dualThetaLower = Var(model.L, domain=NonNegativeReals)
        model.dualGenerationUpper = Var(model.I, domain=NonNegativeReals)
        model.dualGenerationLower = Var(model.I, domain=NonNegativeReals)
        model.dualLostLoad = Var(model.N, domain=NonNegativeReals)
        model.dualGenerationShift = Var(model.I, domain=NonNegativeReals)

        ## Objective Function
        def fo_handler(model):
            return (
                sum(model.Load[b] * (model.dualBalance[b]-model.dualLostLoad[b])
                for b in model.N) -
                sum(model.Generation[i] * model.dualGenerationShift[i]
                for i in model.I) -
                sum((model.Generation[i]+model.ReserveHoldUp[i]) *
                    model.dualGenerationUpper[i]*
                        model.ContingencyStatusGenerator[i] -
                    (model.Generation[i]-model.ReserveHoldDown[i]) *
                    model.dualGenerationLower[i]*
                        model.ContingencyStatusGenerator[i]
                for i in model.I) -
                sum(model.MaxFlow[l] * model.dualFlowUpper[l]*
                        model.ContingencyStatusLine[l] +
                    model.MaxFlow[l] * model.dualFlowLower[l]*
                        model.ContingencyStatusLine[l]
                for l in model.L if model.LInstalled[l]==1) -
                sum(model.MaxFlow[l] * model.BinInvestment[l] *
                        model.dualFlowUpper[l]*model.ContingencyStatusLine[l] +
                    model.MaxFlow[l] * model.BinInvestment[l] *
                        model.dualFlowLower[l]*model.ContingencyStatusLine[l]
                for l in model.L if model.LInstalled[l]==0) -
                sum(self.settings['BigM'] * (
                    (model.dualThetaUpper[l] - model.dualThetaUpper[l]*
                        model.ContingencyStatusLine[l]) +
                    (model.dualThetaLower[l] - model.dualThetaLower[l]*
                        model.ContingencyStatusLine[l]))
                for l in model.L if model.LInstalled[l]==1) -
                sum(self.settings['BigM'] * (
                    (model.dualThetaUpper[l] - model.dualThetaUpper[l]*
                        model.ContingencyStatusLine[l] *
                        model.BinInvestment[l]) +
                    (model.dualThetaLower[l] - model.dualThetaLower[l]*
                        model.ContingencyStatusLine[l] *
                        model.BinInvestment[l]))
                for l in model.L if model.LInstalled[l]==0)
                )
        model.Obj = Objective(rule=fo_handler, sense=maximize)

        def dual_generation(model, i):
            return (
                model.dualBalance[model.INode[i]] +
                model.dualGenerationLower[i] - model.dualGenerationUpper[i] <=
                0
                )
        model.ConstraintDualGeneration = (
            Constraint(model.I, rule=dual_generation))

        def dual_flow(model, l):
            return (
                model.dualBalance[model.LTo[l]] -
                model.dualBalance[model.LFrom[l]] +
                model.dualFlowLower[l] - model.dualFlowUpper[l] +
                model.dualThetaLower[l] - model.dualThetaUpper[l] == 0
                )
        model.ConstraintDualFlow = (
            Constraint(model.L, rule=dual_flow))

        def dual_theta(model, b):
            return (
                sum((model.dualThetaUpper[l]-model.dualThetaLower[l]) /
                    model.Reactance[l]
                for l in model.L if model.LTo[l]==b) -
                sum((model.dualThetaUpper[l]-model.dualThetaLower[l]) /
                    model.Reactance[l]
                for l in model.L if model.LFrom[l]==b) ==0
                )
        model.ConstraintDualTheta = (
            Constraint(model.N, rule=dual_theta))

        def dual_lost_load(model, b):
            return (
                model.CostLostLoad - model.dualBalance[b] +
                model.dualLostLoad[b] >= 0
                )
        model.ConstraintDualLostLoad = (
            Constraint(model.N, rule=dual_lost_load))

        def dual_generation_shift(model, i):
            return (
                model.CostGenerationShift + model.dualBalance[model.INode[i]] +
                model.dualGenerationShift[i] >= 0
                )
        model.ConstraintDualGenerationShift = (
            Constraint(model.I, rule=dual_generation_shift))

        model.dual = Suffix(direction=Suffix.IMPORT)

        self.opt_models['risk_relaxed'] = model

if __name__ == '__main__':
    print('Testing...')
    from system_models.test_small_system import TestSmallSystem
    from system_models.test_small_system_2_gen import TestSmallSystem2Gen
    from system_models.test_small_system_3 import TestSmallSystem3
    from system_models.test_small_system_4 import TestSmallSystem4
    from system_models.test_small_system_no_failures import TestSmallSystemNoFailures
    self = PaperFormulation(clusternum=1)
    self.execute(datasource=TestSmallSystem2Gen())
    # data = TestSmallSystem2Cont().data
    #
    # self.execute(data=data)
    #
    # self.worst_contingencies
    # value(self._last_instance['risk'][1,1].Obj)
    # self._last_instance['risk'][1,1].ProbabilityOccurrence.extract_values()
    # self._last_instance['risk'][1,1].ContingencyStatusGenerator.extract_values()
    # self._last_instance['risk'][1,1].ContingencyStatusLine.extract_values()
    # # self._last_instance['risk'][1,1].INode.extract_values()
    # self._last_instance['investment_operation'].RiskCut.extract_values()
    # self._last_instance['investment_operation'].KImpact.extract_values()
    # # self._last_instance['investment_operation'].BinInvestment.extract_values()
    # # self._last_instance['investment_operation'].BinInvestment.extract_values()
    # self.data[None]
    # # self._risk_input_data(1,1)
# self.data
# self._last_instance['investment_operation'].RiskCut.extract_values()
# self._last_instance['investment_operation'].ConstraintBendersCuts[1,1,2]
# self.data[None]['KImpact'] (1,2,1)
    # import cloudpickle
    # with open('aaa.test', 'wb') as f:  # Python 3: open(..., 'wb')
    #     cloudpickle.dump(self._last_instance['investment_operation'], f)
    # # self.read_solution()
    # with open('aaa.test', 'rb') as f:  # Python 3: open(..., 'wb')
    #     aaa=cloudpickle.load(f)
    # aaa
