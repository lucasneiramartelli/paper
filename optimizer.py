from __future__ import division
from pyomo.environ import *
from os.path import join
import pandas as pd
import cloudpickle

from optimization_models.classic_formulation import ClassicFormulation
from optimization_models.paper_formulation import PaperFormulation

from system_models.test_small_system_no_failures import TestSmallSystem
from system_models.test_small_system_2_gen import TestSmallSystem2Gen
from system_models.test_small_system_3 import TestSmallSystem3
from system_models.test_small_system_4 import TestSmallSystem4
from system_models.IEEE24Busbar.RTS_1 import RTS24IEEE1

class PowerSystemOptimizer():
    def __init__(self):
        self.optimization_models = {
            'classic_formulation': ClassicFormulation,
            'paper_formulation': PaperFormulation,
        }
        self.system_models = {
            'small_1': TestSmallSystem,
            'small_2': TestSmallSystem2Gen,
            'small_3': TestSmallSystem3,
            'small_4': TestSmallSystem4,
            'rts_1': RTS24IEEE1,
        }

    def optimize(self, sys_model, opt_model):
        system_model = self.system_models[sys_model]
        optimization_model = self.optimization_models[opt_model]
        return optimization_model().execute(
            datasource=system_model(),
            )

    def read_solution(self, solution_file):
        with open(solution_file,'rb') as f:  # Python 3: open(..., 'rb')
            solution, settings = cloudpickle.load(f)
        return solution, settings

    def solution_to_df(self,solution):
        out = {}
        results = solution['instance']
        for val in results.component_objects():
            if val.ctype is pyomo.core.base.param.SimpleParam:
                continue
                print(value(val))
                # val.ctype
            elif val.ctype is pyomo.core.base.param.Param:
                continue
                print(val.extract_values())
                # print(value(val))
            elif val.ctype is pyomo.core.base.set.RangeSet:
                continue
                print(val.bounds())
            elif val.ctype is pyomo.core.base.set.Set:
                continue
                print(val.data())
                for dom in val.domain.subsets():
                    print(dom)
            elif val.ctype is pyomo.core.base.var.Var:
                columns = []
                if val.index_set().ctype == pyomo.core.base.set.Set:
                    for idx in val.index_set().subsets():
                        columns += [idx.getname()]
                else:
                    columns += [val.index_set().getname()]
                columns += ['value']
                aa = val.extract_values()
                bb = [list(idx)+[val] if type(idx) is tuple else [idx]+[val] for idx,val in aa.items()]
                out[val.getname()] = pd.DataFrame(
                    columns=columns,
                    data=bb)
                # print(val,val.ctype)
                # print(val.extract_values())
            elif val.ctype is pyomo.core.base.objective.Objective:
                continue
                print(value(val))
            elif val.ctype is pyomo.core.base.constraint.Constraint:
                continue
            elif val.ctype is pyomo.core.base.suffix.Suffix:
                continue
            else:
                aaaa
        return out

    def solution_file(self, sys_model, opt_model):
        system_model = self.system_models[sys_model]().__class__.__name__
        optimization_model = self.optimization_models[opt_model]().__class__.__name__
        return join(
            'solutions',
            optimization_model,
            system_model+'.pso')


if __name__ == '__main__':
    print('Running Power System Optimizer...')
    self = PowerSystemOptimizer()

    execute_list = [
        # {
        # 'system': 'small_1',
        # 'optimization': 'classic_formulation',
        # 'file_name': 'small_1_classic',
        # },
        # {
        # 'system': 'small_1',
        # 'optimization': 'paper_formulation',
        # 'file_name': 'small_1_paper',
        # },
        {
        'system': 'rts_1',
        'optimization': 'classic_formulation',
        'file_name': 'rts_1_classic',
        },
        {
        'system': 'rts_1',
        'optimization': 'paper_formulation',
        'file_name': 'rts_1_paper',
        },
    ]
    for item in execute_list:
        self.optimize(
            item['system'],
            item['optimization']
        )
    #
    solution,settings = self.read_solution(self.solution_file(item['system'],item['optimization']))
    results = self.solution_to_df(solution)
    #
    # out = {}
    # for val in results.component_objects():
    #
    #     if val.ctype is pyomo.core.base.param.SimpleParam:
    #         continue
    #         print(value(val))
    #         # val.ctype
    #     elif val.ctype is pyomo.core.base.param.Param:
    #         continue
    #         print(val.extract_values())
    #         # print(value(val))
    #     elif val.ctype is pyomo.core.base.set.RangeSet:
    #         continue
    #         print(val.bounds())
    #     elif val.ctype is pyomo.core.base.set.Set:
    #         continue
    #         print(val.data())
    #         for dom in val.domain.subsets():
    #             print(dom)
    #     elif val.ctype is pyomo.core.base.var.Var:
    #         print(val,val.ctype)
    #         columns = []
    #         if val.index_set().ctype == pyomo.core.base.set.Set:
    #             for idx in val.index_set().subsets():
    #                 columns += [idx.getname()]
    #         else:
    #             columns += [val.index_set().getname()]
    #         columns += ['value']
    #         print(columns)
    #         aa = val.extract_values()
    #         bb = [list(idx)+[val] if type(idx) is tuple else [idx]+[val] for idx,val in aa.items()]
    #         out[val.getname()] = pd.DataFrame(
    #             columns=columns,
    #             data=bb)
    #         # print(val,val.ctype)
    #         # print(val.extract_values())
    #     elif val.ctype is pyomo.core.base.objective.Objective:
    #         continue
    #         print(value(val))
    #     elif val.ctype is pyomo.core.base.constraint.Constraint:
    #         continue
    #     elif val.ctype is pyomo.core.base.suffix.Suffix:
    #         continue
    #     else:
    #         aaaa
    #     # if val.is_variable_type():
    #     #     print(val.extract_values())
