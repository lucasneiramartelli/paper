import pandas as pd

from utils import parse_data

from system_models.test_small_system import TestSmallSystem

class TestSmallSystemNoFailures(TestSmallSystem):
    def __init__(self):
        super().__init__()
        self.data = None
        self.contingencies = {
            'Probability': { # DIM (S,T)
                },
            'Generators':{ # DIM (I,S)
                },
            'Lines':{ # DIM (L,S)
                },
            }
        self._initialize_data()

if __name__ == '__main__':
    print('Testing...')
    self = TestSmallSystem()

    self.data
