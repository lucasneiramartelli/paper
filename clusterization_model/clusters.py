from random import uniform
from math import log

def create_cluster(*args,**kargs):
    return Clusterization().create_cluster(*args,**kargs)

class Clusterization():
    def __init__(self):
        self.function = None
        self.cluster_centroid = None
        self.cluster_items = None
        self.clusternum = None
        self.work_data = None
        self.data = None


    def create_cluster(self,data,clusternum,function=None):
        self.clusternum = clusternum
        self.function = function
        self.data = data
        if self.function is None:
            self.work_data = self.data
        elif self.function == 'log':
            self.work_data = [log(y,10) for y in self.data]

        self._initialize()
        pre_centroid = None
        for i in range(10000):
            self._assign_centroid()
            self._recalculate_centroid()

            if pre_centroid != None:
                if pre_centroid == self.cluster_centroid:
                    self._fix_centroid()
                    return self.cluster_centroid,self.cluster_items

            pre_centroid = self.cluster_centroid

    def _initialize(self):
        self.cluster_centroid = []
        for i in range(self.clusternum):
            self.cluster_centroid.append(uniform(min(self.work_data),max(self.work_data)))

    def _assign_centroid(self):
        self.cluster_items = [[] for i in range(self.clusternum)]
        for idx,item in enumerate(self.work_data):
            sse = None
            cluster_idx = None
            for c_idx in range(self.clusternum):
                if sse is None or sse > pow(item-self.cluster_centroid[c_idx],2):
                    sse = pow(item-self.cluster_centroid[c_idx],2)
                    cluster_idx = c_idx
            self.cluster_items[cluster_idx].append(idx)

    def _recalculate_centroid(self):
        self.cluster_centroid = []
        for i in range(self.clusternum):
            self.cluster_centroid.append(
                sum([self.work_data[idx] for idx in self.cluster_items[i]])/len(self.cluster_items[i]))

    def _fix_centroid(self):
        self.cluster_centroid = []
        for i in range(self.clusternum):
            self.cluster_centroid.append(
                sum([self.data[idx] for idx in self.cluster_items[i]])/len(self.cluster_items[i]))

if __name__ == '__main__':
    print('Testing...')

    create_cluster(
        data=[0.1],
        clusternum=1,
        function = 'log',
    )
