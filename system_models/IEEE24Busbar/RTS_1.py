from os.path import join
import pandas as pd

from utils import parse_data

from system_models.system_model_base import SystemModelBase

class RTS24IEEE1(SystemModelBase):
    def __init__(self, style = 'conditional'):
        super().__init__()
        self.style = style
        self._initialize_data()

    def _initialize_data(self):
        self.load_from_excel(join('system_models','IEEE24Busbar','RTS_1.xlsx'))
        # self.load_from_excel(join('RTS_1.xlsx'))
        self.probability_from_outagerates(style = self.style)
        real_data = {}
        real_data['Ie'] = parse_data(
            len(self.generators['Node']))
        real_data['Te'] = parse_data(
            len(self.nodes['sample_duration']))
        real_data['Le'] = parse_data(
            len(self.lines['Installed']))
        real_data['Se'] = parse_data(
            len(self.contingencies['Probability']))
        real_data['Ne'] = parse_data(
            len(self.nodes['load']))
        real_data['LTo'] = parse_data(
            self.lines['To'])
        real_data['LFrom'] = parse_data(
            self.lines['From'])
        real_data['LInstalled'] = parse_data(
            self.lines['Installed'])
        real_data['INode'] = parse_data(
            self.generators['Node'])
        real_data['CostVariable'] = parse_data(
            self.generators['VariableCost'])
        real_data['CostReserveHoldUp'] = parse_data(
            self.generators['CostReserveHoldUp'])
        real_data['CostReserveHoldDown'] = parse_data(
            self.generators['CostReserveHoldDown'])
        real_data['MaxReserveHoldUp'] = parse_data(
            self.generators['MaxReserveHoldUp'])
        real_data['MaxReserveHoldDown'] = parse_data(
            self.generators['MaxReserveHoldDown'])
        real_data['MaxCapacity'] = parse_data(
            self.generators['MaxCapacity'])
        real_data['MinStableLevel'] = parse_data(
            self.generators['MinStableLevel'])
        real_data['CostGenerationShift'] = parse_data(
            self.cost_generation_shift)
        real_data['MaxFlow'] = parse_data(
            self.lines['MaxFlow'])
        real_data['Reactance'] = parse_data(
            self.lines['Reactance'])
        real_data['CostTransmissionInvestment'] = parse_data(
            self.lines['InvestmentCost'])
        real_data['ProbabilityOccurrence'] = parse_data(
            self.contingencies['Probability'])
        real_data['ContingencyStatusGenerator'] = parse_data(
            self.contingencies['Generators'])
        real_data['ContingencyStatusLine'] = parse_data(
            self.contingencies['Lines'])
        real_data['CostLostLoad'] = parse_data(
            self.cost_lost_load)
        real_data['Load'] = parse_data(
            self.nodes['load'])
        real_data['SampleDuration'] = parse_data(
            self.nodes['sample_duration'])
        self.data = {None: real_data}

if __name__ == '__main__':
    print('Testing...')
    self = RTS24IEEE1()

    self.data[None]['Se']
    self.contingencies['Generators']
